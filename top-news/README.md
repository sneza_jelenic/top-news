# Top news app

## Project structure
* On the header there are three links for pages on left side and ability for changing language on right side
* **Top news page** - This is the page with all top news for selected country
* **Categories page** - On this page there are five categories and top five news by category
** All categories can be expanded/collapsed
** If category is expanded, 5 top news are shown to user and more button that leads to new page with all top news for that category
** **Search page** - the page with list of top news and search bar where news can be searched

## Cloning and setting up
To run the app after cloning the repository the following steps are necessary:
* Install [node](https://nodejs.org/en/download/current/) on your computer
* Run **npm install - g @angular/cli** to instal Angular CLI on your computer
* Clone the project
* Go to project folder and run **npm install** to download all the packages defined in package.json.
* **ng serve** to start the application.
* For running test, run command: **ng test**

