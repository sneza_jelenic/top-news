import { HttpClientModule } from '@angular/common/http';
import { TestModuleMetadata } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeatureModule } from './features/feature.module';
import { MaterialModule } from './material.module';
import { SharedModule } from './shared/shared.module';
import { NavbarComponent } from './core/components/navbar/navbar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

export class TestConfig {
    moduleDef: TestModuleMetadata = {
        declarations: [
            AppComponent,
            NavbarComponent
          ],
          imports: [
            BrowserModule,
            AppRoutingModule,
            BrowserAnimationsModule,
            MaterialModule,
            HttpClientModule,
            SharedModule,
            FeatureModule,
            FormsModule,
            HttpClientTestingModule
          ],
    };
  }