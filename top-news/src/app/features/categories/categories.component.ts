import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subject, Subscription } from 'rxjs';
import { skip } from 'rxjs/operators';
import { NewsService } from 'src/app/core/services/news.service';
import { DialogNewsDetailsComponent } from 'src/app/shared/components/dialog-news-details/dialog-news-details.component';
import { NewsRequestModel } from 'src/app/shared/models/news-request.model';
import { News } from 'src/app/shared/models/news.model';
import { defaultImageUrl } from 'src/app/shared/utilities/constants';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit, OnDestroy {
  categories = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];
  newsByCategory$: Subject<any[]> = new Subject();
  country: string;
  private subscription: Subscription = new Subscription();

  constructor(private newsService: NewsService,
              private dialog: MatDialog) { }

  ngOnInit() {
    const countrySub = this.newsService.selectedCountry$.subscribe(country => {
      if (country) {
        this.country = country === 'gb' ? 'Great Britain' : 'United States';
        this.getNewsByCategory();
      }
    });
    this.subscription.add(countrySub);
  }

  getNewsByCategory() {
    this.newsService.resetNewsByCategory();
    const searchRequest = new NewsRequestModel();
    this.categories.map(category => {
      searchRequest.category = category;
      return this.newsService.getTopNews(searchRequest, true).toPromise();
    });
    this.newsService.newsByCategory$.pipe(skip(this.categories.length)).subscribe(res => {
      this.newsByCategory$.next(res);
    });
  }

  onImgError(event) {
    event.target.src = defaultImageUrl;
  }

  openNewsDetails(item: News) {
    this.dialog.open(DialogNewsDetailsComponent, { data: item, width: '1000px' });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
