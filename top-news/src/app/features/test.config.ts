import {TestModuleMetadata} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { CategoriesComponent } from './categories/categories.component';
import { TopNewsComponent } from './top-news/top-news.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

export class TestConfig {
  moduleDef: TestModuleMetadata = {
    declarations: [
        TopNewsComponent,
        CategoriesComponent
        ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        FormsModule,
        SharedModule,
        RouterModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
  };
}
