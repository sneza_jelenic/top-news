import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { NewsService } from 'src/app/core/services/news.service';
import { DialogNewsDetailsComponent } from 'src/app/shared/components/dialog-news-details/dialog-news-details.component';
import { News } from 'src/app/shared/models/news.model';
import * as _ from 'lodash';
import { NewsRequestModel } from 'src/app/shared/models/news-request.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-top-news',
  templateUrl: './top-news.component.html',
  styleUrls: ['./top-news.component.scss']
})
export class TopNewsComponent implements OnInit, OnDestroy {
  topNews$: Observable<any>;
  title: string;
  country: string;
  filterNews = _.debounce(() => this.getNews(), 300);
  searchTerm: string;
  page: string;
  category: string;
  private subscription: Subscription = new Subscription();

  constructor(private newsService: NewsService,
              private dialog: MatDialog,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.page = this.route.snapshot.data.page;
    this.category = this.route.snapshot.params.name;
    this.topNews$ = this.newsService.topNews$;
    const countrySub = this.newsService.selectedCountry$.subscribe(country => {
      if (country) {
        this.country = country === 'gb' ? 'Great Britain' : 'United States';
        this.getNews();
        this.getTitle();
      }
    });
    this.subscription.add(countrySub);
  }

  getNews() {
    let searchRequest: NewsRequestModel;
    if (this.searchTerm) {
      searchRequest = new NewsRequestModel();
      searchRequest.q = this.searchTerm;
    }
    if (this.category) {
      searchRequest = new NewsRequestModel();
      searchRequest.category = this.category;
    }
    this.newsService.getTopNews(searchRequest).toPromise();
  }

  openNewsDetails(item: News) {
    this.dialog.open(DialogNewsDetailsComponent, { data: item, width: '1000px' });
  }

  ngOnDestroy() {
    this.newsService.resetTopNews();
    this.subscription.unsubscribe();
  }

  getTitle() {
    switch (this.page) {
      case 'search':
        this.title = `Search top news from ${this.country} by term:`;
        break;
      case 'category-info':
        this.title = `Top ${this.category} news from ${this.country}:`;
        break;
      default:
        this.title = ` Top news from ${this.country}:`;
        break;
    }
  }
}
