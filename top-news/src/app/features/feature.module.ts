import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { CategoriesComponent } from './categories/categories.component';
import { TopNewsComponent } from './top-news/top-news.component';

@NgModule({
  declarations: [
    TopNewsComponent,
    CategoriesComponent
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    TopNewsComponent,
    CategoriesComponent
  ]
})
export class FeatureModule { }
