import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { CategoriesComponent } from './features/categories/categories.component';
import { TopNewsComponent } from './features/top-news/top-news.component';

const routes: Routes = [
    { path: '', redirectTo: 'top-news', pathMatch: 'full' },
    { path: 'top-news', component: TopNewsComponent, data: { page: 'top-news'} },
    { path: 'search', component: TopNewsComponent, data: { page: 'search'} },
    {
      path: 'categories',
      children: [
        { path: '',  component: CategoriesComponent, },
        { path: 'category/:name', component: TopNewsComponent, data: { page: 'category-info'} }
      ]
    },
];
const routerExtraOptions: ExtraOptions = {
  scrollPositionRestoration: 'top'
};
@NgModule({
  imports: [RouterModule.forRoot(routes, routerExtraOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
