import { TestBed, inject } from '@angular/core/testing';
import { TestConfig } from 'src/app/test.config';
import { NewsService } from './news.service';

describe('Service: News', () => {
  beforeEach(() => {
    TestBed.configureTestingModule(new TestConfig().moduleDef).compileComponents();
  });

  it('should ...', inject([NewsService], (service: NewsService) => {
    expect(service).toBeTruthy();
  }));
});
