import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { NewsRequestModel } from 'src/app/shared/models/news-request.model';
import { createQueryString } from 'src/app/shared/utilities/query.utility';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  // private readonly apiKey = '20e4de56d7c8405ab8927e6c993419ad';
  private readonly apiKey = 'acc6ea28ecb14700bd06f19d36ad9388';
  private readonly url = 'https://newsapi.org/v2/top-headlines';

  private selectedCountrySubject$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public selectedCountry$: Observable<any> = this.selectedCountrySubject$.asObservable();

  private topNewsSubject$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public topNews$: Observable<any> = this.topNewsSubject$.asObservable();

  private newsByCategorySubject$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public newsByCategory$: Observable<any> = this.newsByCategorySubject$.asObservable();

  constructor(private http: HttpClient) { }

  public getTopNews(options?: NewsRequestModel, categories?: boolean): Observable<any> {
    const request = new NewsRequestModel();
    request.country = this.selectedCountrySubject$.value;
    let query = `?${createQueryString(request)}`;
    const category = options ? options.category : null;
    if (options) {
      query += `&${createQueryString(options)}`;
    }
    return this.http.get(`${this.url}${query}&apiKey=${this.apiKey}`)
    .pipe(map((response: any) => {
      if (categories) {
        const currentNewsByCategory = this.newsByCategorySubject$.value ?  this.newsByCategorySubject$.value : [];
        currentNewsByCategory.push({ category, articles: response.articles.slice(0, 5) });
        this.newsByCategorySubject$.next(currentNewsByCategory);
      } else {
        this.topNewsSubject$.next(response.articles);
      }
      return response;
    }));
  }

  setSelectedCountry(selectedCountry: string) {
    this.selectedCountrySubject$.next(selectedCountry);
  }

  resetNewsByCategory() {
    this.newsByCategorySubject$.next(null);
  }

  resetTopNews() {
    this.topNewsSubject$.next(null);
  }
}
