import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NewsService } from '../../services/news.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  isSmallScreen: boolean;
  activeCountry: string;
  countries = ['us', 'gb'];
  activeLink: string;
  links: Link[] = [
    {
      name: 'Top news',
      path: 'top-news'
    },
    {
      name: 'Categories',
      path: 'categories'
    },
    {
      name: 'Search',
      path: 'search'
    }
  ];

  constructor(private breakPointObserver: BreakpointObserver,
              private newsService: NewsService,
              private router: Router) { }

  ngOnInit() {
    this.smallScreenCheck();
    this.activeCountry = this.countries[0];
    this.newsService.setSelectedCountry(this.activeCountry);
    const routerSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.activeLink = window.location.pathname.split('/')[1];
      }
    });
    this.subscription.add(routerSubscription);
  }

  smallScreenCheck() {
    this.breakPointObserver.observe([Breakpoints.Small, Breakpoints.Medium, Breakpoints.Large])
    .subscribe(() => {
      this.isSmallScreen = this.breakPointObserver.isMatched('(max-width: 599px)');
    });
  }

  changeLanguage(selected: string) {
    this.activeCountry = selected;
    this.newsService.setSelectedCountry(this.activeCountry);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

export interface Link {
  name: string;
  path: string;
}
