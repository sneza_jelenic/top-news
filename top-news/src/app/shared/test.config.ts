import {TestModuleMetadata} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { DialogNewsDetailsComponent } from './components/dialog-news-details/dialog-news-details.component';
import { NewsGridComponent } from './components/news-grid/news-grid.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

export class TestConfig {
  moduleDef: TestModuleMetadata = {
    declarations: [
        DialogNewsDetailsComponent,
        NewsGridComponent
      ],
      imports: [
        MaterialModule,
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        FormsModule,
        HttpClientTestingModule
      ],
  };
}
