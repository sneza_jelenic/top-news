import { Component, Input } from '@angular/core';
import { News } from '../../models/news.model';
import { defaultImageUrl } from '../../utilities/constants';

@Component({
  selector: 'app-news-grid',
  templateUrl: './news-grid.component.html',
  styleUrls: ['./news-grid.component.scss']
})
export class NewsGridComponent {
  @Input() newsList: News;
  @Input() openNewsDetailsHandler: (item: News) => void;

  onImgError(event) {
    event.target.src = defaultImageUrl;
  }

  openNewsDetails(item: News) {
    this.openNewsDetailsHandler(item);
  }
}
