import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestConfig } from '../../test.config';
import { NewsGridComponent } from './news-grid.component';

describe('NewsGridComponent', () => {
  let component: NewsGridComponent;
  let fixture: ComponentFixture<NewsGridComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule(new TestConfig().moduleDef).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
