import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestConfig } from '../../test.config';
import { DialogNewsDetailsComponent } from './dialog-news-details.component';

describe('DialogNewsDetailsComponent', () => {
  let component: DialogNewsDetailsComponent;
  let fixture: ComponentFixture<DialogNewsDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule(new TestConfig().moduleDef).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogNewsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
