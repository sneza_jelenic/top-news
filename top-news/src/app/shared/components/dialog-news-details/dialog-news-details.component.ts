import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { News } from '../../models/news.model';

@Component({
  selector: 'app-dialog-news-details',
  templateUrl: './dialog-news-details.component.html',
  styleUrls: ['./dialog-news-details.component.scss']
})
export class DialogNewsDetailsComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public newsDetails: News,
              public dialogRef: MatDialogRef<DialogNewsDetailsComponent>) { }

  closeDialog() {
    this.dialogRef.close(null);
  }
}
