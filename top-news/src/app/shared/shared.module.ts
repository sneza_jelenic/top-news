import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { DialogNewsDetailsComponent } from './components/dialog-news-details/dialog-news-details.component';
import { NewsGridComponent } from './components/news-grid/news-grid.component';

@NgModule({
  declarations: [
    DialogNewsDetailsComponent,
    NewsGridComponent
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  exports: [
    NewsGridComponent
  ],
  entryComponents: [
    DialogNewsDetailsComponent
  ]
})
export class SharedModule { }
