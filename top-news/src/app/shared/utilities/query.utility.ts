export function createQueryString(options: any) {
  const queryArray = [];
  Object.keys(options).filter(key => _validateQueryParamValue(key, options)).forEach(key => {
    queryArray.push(`${key}=${options[key]}`);
  });
  return queryArray.join('&');
}

function _validateQueryParamValue(key, options) {
  if (options[key] === undefined) {
    return false;
  }
  if (options[key] === '' || options[key] === ' ') {
    return false;
  }
  return true;
}
